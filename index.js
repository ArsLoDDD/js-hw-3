'use strict'

//1. Циклы позволяют перебирать варианты, делать валидации, ну или например добавлять определенные значения или например классы эллементам
//2. Ну фор лучше где надо чтото считать, вайл удобен для валидации
//3. Явное это когда ты сам преобразовываешь тип определенными инструментами, а неявное это когда джаваскрипт изза своей слабой типизации под капотом в определенных случаях приводит один тип в другой


let frstNum = prompt('Write the lowest number');
let scdNum = prompt('Write the highest number');
while ((isNaN(+frstNum) || isNaN(+scdNum)) === true || (frstNum || scdNum) === '' || +scdNum <= +frstNum || frstNum < 0) {
  alert(`Write correct number! *${frstNum}* and *${scdNum}* is wrong!`)
  frstNum = prompt('Write the lowest number');
  scdNum = prompt('Write the highest number');
}
const findAllMultipleNum = (m = +frstNum, n = +scdNum) => {
  let countInteger = 0;
  for (let i = m; i < n + 1; i++) {
    if (i % 5 === 0 && i !== 0) {
      console.log(i);
      countInteger++
    }
  }
  if (countInteger === 0) {
    return console.log('Sorry, no numbers');
  } else {
    return `Count of numbers divided by 5 without remainder: ${countInteger}`
  }
}
const findAllSimpleNum = (m = +frstNum, n = +scdNum) => {
  let countSimpleNum = 0;
  let countNum = 0;
  for (let i = m; i < n + 1; i++) {
    for (let j = 2; j < n + 1; j++) {
      if (i % j === 0) {
        countNum++
      }
    }
    if (countNum === 1) {
      console.log(i);
      countSimpleNum++
    }
    countNum = 0;
  }
  if (countSimpleNum === 0) {
    return console.log('Sorry, no numbers');
  }
}
findAllSimpleNum()
